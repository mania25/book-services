# Books API

# Requirement

- Go 1.9+
- MongoDB 2.x
- Dep

# Setup

- Enter to Golang Project Source Directory `cd $GOPATH/src/`
- Clone this repository using command: `git clone git@gitlab.com:mania25/book-services.git`
- Enter directory from cloned repository by using command `cd book-services`
- Run `dep ensure`
- Create global configuration file for the service by renaming config.toml.example to config.toml
- Fill in the blank configuration on config.toml
- Run Books Service `go run main.go`
- Happy coding

# Design Pattern

- M(odel)V(iew)C(ontroller)
- Model : object yang digunakan oleh controller
- Controller : logic untuk API
- View : object response
