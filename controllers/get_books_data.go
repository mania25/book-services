package controllers

import (
	"book-services/models"
	"book-services/utils/database/MongoDB"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	Index "book-services/views"

	"github.com/gin-gonic/gin"
)

func GetBooksData(c *gin.Context) {
	pageNumber := c.Query("page[number]")
	pageSize := c.Query("page[size]")
	sort := c.Query("sort")
	query := c.Query("query")
	status := c.Query("filter[status]")

	var response = &Index.DefaultResponseFormat{}

	var books []models.Books

	connection, errMongoDB := MongoDB.ConnectMongoDB()

	if errMongoDB != nil {
		panic(errMongoDB)
	}

	bsonQueryRegex := bson.M{"$regex": bson.RegEx{Pattern: query, Options: "gi"}}

	bookResults := connection.Collection("books").Find(bson.M{
		"title":  bsonQueryRegex,
		"status": status,
	})

	bookResults.Query.Sort(sort)

	if validateInteger(pageNumber) && validateInteger(pageSize) {
		pageSizeInteger, _ := strconv.Atoi(pageSize)
		pageNumberInteger, _ := strconv.Atoi(pageNumber)

		paginateInfo, _ := bookResults.Paginate(pageSizeInteger, pageNumberInteger)

		response.Meta.Page = paginateInfo.Current
		response.Meta.Size = paginateInfo.PerPage
		response.Meta.Count = paginateInfo.RecordsOnPage
		response.Meta.TotalPages = paginateInfo.TotalPages
		response.Meta.TotalData = paginateInfo.TotalRecords

	} else {
		response.Code = http.StatusBadRequest
		response.Status = "Invalid Parameter Page Size/Page Number, Must Be Integer"

		c.JSON(http.StatusBadRequest, response)
		return
	}

	bookResults.Query.All(&books)

	response.Code = http.StatusOK
	response.Status = "success"
	response.Data = &books

	c.JSON(http.StatusOK, response)
}

func validateInteger(data string) bool {
	_, err := strconv.Atoi(data)

	return err == nil
}
