package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetAPIVersion is a gin handler to return current API Version
func GetAPIVersion(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"version": "0.0.1"})
}
