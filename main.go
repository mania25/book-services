package main

import (
	"fmt"
	"net/http"

	"book-services/controllers"
	Index "book-services/views"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigType("toml")
	viper.SetConfigName("config")
	viper.AddConfigPath("./config")
	viper.AddConfigPath("$GOPATH/src/book-services/config")

	errReadingConfigFile := viper.ReadInConfig()

	if errReadingConfigFile != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", errReadingConfigFile))
	}

	if viper.GetString("mode") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	engine := gin.Default()
	engine.RedirectTrailingSlash = false

	v1 := engine.Group("/api/v1")

	v1.GET("/", controllers.GetAPIVersion)
	v1.GET("/books", controllers.GetBooksData)

	engine.NoRoute(func(c *gin.Context) {
		var response = &Index.DefaultResponseFormat{
			Status: "Not Found",
			Code:   http.StatusNotFound,
		}

		c.JSON(http.StatusNotFound, response)
	})

	engine.Run(":8080")
}
