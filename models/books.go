package models

type Books struct {
	CustomDocumentBase `bson:",inline"`
	Title              string `json:"title"`
	Price              int64  `json:"price"`
	Status             string `json:"status"`
}
