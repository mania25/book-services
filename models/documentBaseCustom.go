package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type CustomDocumentBase struct {
	Id       bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Created  time.Time     `bson:"-" json:"-"`
	Modified time.Time     `bson:"-" json:"-"`

	// We want this to default to false without any work. So this will be the opposite of isNew. We want it to be new unless set to existing
	exists bool
}

// Satisfy the new tracker interface
func (d *CustomDocumentBase) SetIsNew(isNew bool) {
	d.exists = !isNew
}

func (d *CustomDocumentBase) IsNew() bool {
	return !d.exists
}

// Satisfy the document interface
func (d *CustomDocumentBase) GetId() bson.ObjectId {
	return d.Id
}

func (d *CustomDocumentBase) SetId(id bson.ObjectId) {
	d.Id = id
}

func (d *CustomDocumentBase) SetCreated(t time.Time) {
	d.Created = t
}

func (d *CustomDocumentBase) SetModified(t time.Time) {
	d.Modified = t
}
