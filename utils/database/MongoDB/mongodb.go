package MongoDB

import (
	"fmt"

	"github.com/go-bongo/bongo"
	"github.com/spf13/viper"
)

func ConnectMongoDB() (*bongo.Connection, error) {
	viper.SetConfigType("toml")
	viper.SetConfigName("config")
	viper.AddConfigPath("./config")
	viper.AddConfigPath("$GOPATH/src/book-services/config")

	errReadingConfigFile := viper.ReadInConfig()

	if errReadingConfigFile != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", errReadingConfigFile))
	}

	config := &bongo.Config{
		ConnectionString: fmt.Sprintf("%s:%s@%s:%s/%s",
			viper.GetString(fmt.Sprintf("%s.database.username", viper.GetString("mode"))),
			viper.GetString(fmt.Sprintf("%s.database.password", viper.GetString("mode"))),
			viper.GetString(fmt.Sprintf("%s.database.host", viper.GetString("mode"))),
			viper.GetString(fmt.Sprintf("%s.database.port", viper.GetString("mode"))),
			viper.GetString(fmt.Sprintf("%s.database.dbname", viper.GetString("mode"))),
		),
	}

	return bongo.Connect(config)
}
